import json
import requests
from flask import Flask, request, render_template
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from score import Score

app = Flask(__name__)

@app.route("/")
def index():
    print(dir(request))
    print(request.data)
    print(request.values)
    
    return "Hello World!"

@app.route("/<username>", methods=['GET', 'POST'])
def hello(username):
    print("username: %s" % username)
    print(dir(request))
    print(request.data)
    print(request.values)
    
    return "Hello World! %s" % username

@app.route("/test")
def test():
    resp = requests.get("https://api.ipify.org?format=json")
    return resp.content
    
@app.route("/score")
def score():
    engine = create_engine('sqlite:///../jours_3/mydb2.sqlite')
    Session = sessionmaker(bind=engine)
    session = Session()
    scores = session.query(Score).all()
    
    return render_template('index.html', scores=scores)

@app.route("/test_2")
def test_2():
    return None
