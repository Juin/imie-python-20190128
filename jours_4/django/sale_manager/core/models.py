from django.db import models

    
class Customer(models.Model):
    name = models.CharField(
        max_length=128, blank=True, null=True,
        verbose_name="Nom")
    last_name = models.CharField(
        max_length=128, blank=True, null=True,
        verbose_name="Prénom")

    def __str__(self):
        return f"{self.name} {self.last_name}"
    
class Item(models.Model):
    name = models.CharField(
        max_length=128, blank=True, null=True,
        verbose_name="Désignation")
    
class Order(models.Model):
    customer = models.ForeignKey(
        'Customer', blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name="Client")

class OrderLine(models.Model):
    order = models.ForeignKey(
        'Order', blank=True, null=True,
        verbose_name="Commande",
        on_delete=models.CASCADE,
        related_name="lines")
    quantity = models.FloatField(verbose_name="Quantitée")
    price_per_unit = models.FloatField(verbose_name="PU")

