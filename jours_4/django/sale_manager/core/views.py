from django.shortcuts import render
from django.http import HttpResponse
from core.models import Customer, Order


def home(request):
    
    context = {
        "cle": "valeur",
        "customers": Customer.objects.all(),
        "orders": Order.objects.all()
    }
    
    return render(request, "index.html", context)
