from django.contrib import admin
from core.models import Customer, Item, Order, OrderLine

class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name',)

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Item)
admin.site.register(Order)
admin.site.register(OrderLine)

