import json
from pprint import pprint


class Grid:

    def __init__(self, config_file_path=None):
        self.config = self.load_config(config_file_path)
        self.cells = self.initialize_grid()
        self.size_x = self.config["grid"]["size"]["x"]
        self.size_y = self.config["grid"]["size"]["y"]
        self.ships = []
        
    def __str__(self):
        line = list(map( lambda x: None, range(self.size_x)))
        out = list(map(lambda x: list(line), range(self.size_y)))
        for x, y in self.cells.keys():
            if self.cells[(x,y)] is not None:
                out[x][y] = self.cells[(x,y)]
            elif self.is_ship_there((x, y)):
                if self.get_ship((x, y)).has_drawn:
                    out[x][y] = "*"
                else:
                    out[x][y] = "X"
            else:
                out[x][y] = " "
                
        out.insert(0, list(map(lambda x: str(x), range(10))))
        out = [chr(index+64)+"-> "+"|".join(line) for index, line in enumerate(out)]
        out = "\n".join(out)
        return out
    
    def load_config(self, config_file_path):
        with open(config_file_path, 'rt') as cfg:
            config = json.load(cfg)
        
        return config

    def initialize_grid(self):
        ret = {}
        for x in range(self.config["grid"]["size"]["x"]):
            for y in range(self.config["grid"]["size"]["y"]):
                ret[(x, y)] = None
        return ret
        

    def put_ships(self, ships):
        self.ships = ships

    def is_ship_there(self, coords):
        ret = False
        for ship in self.ships:
            if coords in ship.cells:
                ret = True
                break
        return ret

    def get_ship(self, coords):
        ret = None
        for ship in self.ships:
            if coords in ship.cells:
                ret = ship
                break
        return ret
    
    def get_ship_in_range(self, coords, range_size):
        chips = []
        for i in [-range_size,0,range_size]:
            for j in [-range_size,0,range_size]:
                offseted_coords = (coords[0] + i, coords[1] + j)
                chip = self.get_ship(offseted_coords)
                if chip is not None and chip not in chips:
                    chips.append(chip)
    
        return chips

    def cell_is_ok_for_ship(self, ship, coords, range_size=1):
        for i in range(-range_size, range_size+1):
            for j in range(-range_size, range_size+1):
                
                offseted_coords = (coords[0] + i, coords[1] + j)
                if offseted_coords[0] < 0 or offseted_coords[1] < 0 or \
                   offseted_coords[0] >= self.size_x or offseted_coords[1] >= self.size_y:
                    return False
                ship_found = self.get_ship(offseted_coords)
                if ship_found is not None and ship_found != ship:
                    return False

        return True

    def add_shoot(self, coords, is_ploof):
        self.cells[coords] = "0" if is_ploof else "X"
