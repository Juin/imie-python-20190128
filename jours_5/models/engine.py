import random
import re

from models.player import Player


class Engine:

    def __init__(self, config_file_path=None, no_prompt=False):
        self.opponent = None
        self.winner = None
        self.no_prompt = no_prompt
        self.players = self.initialize(config_file_path)
        self.current_player = random.choice(self.players) if no_prompt else None
        
        
    def initialize(self, config_file_path):
        
        players = []
        if self.no_prompt:
            player_name = "Player 1 name"
        else:
            player_name = input("Player 1 name?\n")
        players.append(Player(player_name,
                              config_file_path=config_file_path))
        if not self.no_prompt:
            player_name = input("Player 2 name?\n")
        players.append(Player(player_name,
                              config_file_path=config_file_path))
        return players 
    
    def start(self):
        while self.winner is None:
            
            if self.current_player is None:
                self.current_player = random.choice(self.players)
            else:
                self.current_player = self.opponent
            self.opponent = self.players[(self.players.index(self.current_player)+1) % 2]

            for p in [self.opponent, self.current_player]:
                print("%s Grid Boat" % p)
                print(p.grid)
                print("%s Grid Shoot" % p)
                print(p.shots_grid)
                       
            position = None
            while position is None:
                position = input("Player %s choose a target\n" % self.current_player)
                matches = re.match('([A-Z])([0-9])', position)
                if matches is not None:
                    line, column = matches.groups()
                    column = int(column)
                    line = ord(line) - 65
                    position = (line, column)

            is_ploof = self.opponent.shoot_from_ennemi(position)
            self.current_player.shoot_to_ennemi(position, is_ploof)

            winner = self.current_player
            for ship in self.opponent.ships:
                if not ship.has_drawn:
                    winner = None
                    
            self.winner = winner

        print("The winner is %s" % self.winner)
