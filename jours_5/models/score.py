from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

Base = declarative_base()


class Score(Base):
    __tablename__ = 'high_score'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    score = Column(Integer)

    def __repr__(self):
        return f"{self.name} : {self.score} pts"

    @property
    def name_id(self):
        return "{}{}".format(self.name, self.id)
