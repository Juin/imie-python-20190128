from pprint import pprint

def une_fonction(arg):
    return "AA"

class MaClass:

    def meth(self):
        print("meth", self)
    
    @classmethod
    def cls_meth(cls):
        print("cls_meth", cls)

if __name__ == "__main__":

    instance = MaClass()
    instance.cls_meth()
    instance.meth()
    MaClass.cls_meth()
