import argparse
import logging

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from prompt_toolkit import Application

from models.grid import Grid 
from models.engine import Engine
from models.score import Score
from app import MyApp, before_render

logging.basicConfig(filename='debug.log',level=logging.DEBUG)
logger = logging.getLogger(__name__)


def init_db():
    engine = create_engine('sqlite:///mydb2.sqlite')
    connection = engine.connect()
    ret = connection.execute(
        "CREATE TABLE IF NOT EXISTS high_score ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "name VARCHAR,"
        "score INTEGER);")
    ret = connection.execute(
        "INSERT INTO high_score (name, score) VALUES"
        " ('toto', 200);")
    ret = connection.execute(
        "INSERT INTO high_score (name, score) VALUES"
        " ('rob', 10);")
    connection.close()

    
def show_high_score():
    engine = create_engine('sqlite:///mydb2.sqlite')
    connection = engine.connect()
    rows = connection.execute("SELECT * FROM high_score;")
    for row in rows:
        print("{1}: {2} pts".format(*row))

    connection.close()


def show_high_score_via_orm():
    engine = create_engine('sqlite:///mydb2.sqlite')
    Session = sessionmaker(bind=engine)
    session = Session()
    scores = session.query(Score).all()
    for s in scores:
        print(s)
        print(s.name_id)

        
if __name__ == "__main__":
    logger.debug("MAIN")

    parser = argparse.ArgumentParser(
        description='Yet another naval battle game')

    parser.add_argument('-c', type=str,
                        dest='config',
                        default='config.json',
                        help="config file path")
    parser.add_argument('-s', dest='show_high_score',
                        action='store_true',
                        help="Show high score")    
    parser.add_argument('-a', dest='show_high_score_orm',
                        action='store_true',
                        help="Show high score")    
    parser.add_argument('-i', dest='init_db',
                        action='store_true',
                        help="Init db")    
    parser.add_argument('-p', dest='prompt_toolkit_mode',
                        action='store_true',
                        help="Start with prompt_toolkit")    

    args = parser.parse_args()

    if args.init_db:
        init_db()
    elif args.show_high_score:
        show_high_score()
    elif args.show_high_score_orm:
        show_high_score_via_orm()
    elif args.prompt_toolkit_mode:
        logger.debug("Start with prompt_toolkit_mode on")
        e = Engine(config_file_path=args.config, no_prompt=True)
        app = MyApp(before_render=before_render, engine=e)
        app.run()        
    else:
        e = Engine(config_file_path=args.config)
        e.start()
