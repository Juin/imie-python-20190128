import logging

from prompt_toolkit import Application
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.layout.containers import HSplit, VSplit, Window
from prompt_toolkit.layout.controls import BufferControl, FormattedTextControl
from prompt_toolkit.layout.layout import Layout

from keybinding import kb


logger = logging.getLogger(__name__)

class MyApp(Application):
    def __init__(self, *args, **kwargs):
        
        self.buffer1 = Buffer()

        self.left_grid_pan = FormattedTextControl(text='LEFT')
        self.right_grid_pan = FormattedTextControl(text='RIGHT')
        self.bottom_grid_pan = FormattedTextControl(text='BOTTOM')
        self.top_grid_pan = FormattedTextControl(text='TOP')
        
        self.root_container = HSplit([
            Window(height=1, content=self.top_grid_pan),
            VSplit([
                Window(height=1, char='═'),
                Window(height=1, width=1, char='╦'),
                Window(height=1, char='═')
            ]),
            VSplit([
                Window(content=self.left_grid_pan),
                Window(width=1, char='║'),
                Window(content=self.right_grid_pan),
            ]),
            VSplit([
                Window(height=1, char='═'),
                Window(height=1, width=1, char='╩'),
                Window(height=1, char='═')
            ]),
            Window(height=1, content=self.bottom_grid_pan),
            Window(height=1, char='═'),
            Window(height=1, content=BufferControl(buffer=self.buffer1))
        ])

        self.layout = Layout(self.root_container)

        super().__init__(
            *args,
            layout=self.layout,
            key_bindings=kb,
            full_sceen=True,
            **kwargs)


def before_render(*args, **kwargs):
    logger.debug("before_render %s" % args)
    
