import unittest
import os
import importlib.util
import re
import inspect
import abc
from pprint import pprint


class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)


def slugify(path):
    return re.sub('[^a-zA-Z]', '', path)

def check_exo1(mod, result):
    try:
        ret = mod.double(3, 2)
        assert ret == 6, 'function double is not conform'
        result['score'] += 1
    except Exception as e:
        result["errors"].append("1 - %s" % e)


def check_exo2(univers, result):
    try:
        assert inspect.isclass(univers.LifeBeing)
        assert abc.ABC in inspect.getmro(univers.LifeBeing) , 'LifeBeing is not a sub class of abc.ABC'
        result['score'] += 1
    except Exception as e:
        result["errors"].append("2 - %s" % e)

        
def check_exo3(univers, result):
    try:
        lb = univers.Human()
        assert hasattr(lb, 'weight'), "instance of LifeBeing has no attribut weight" 
        result['score'] += 1
    except Exception as e:
        result["errors"].append("3 - %s" % e)


def check_exo4(univers, result):
    try:
        lb = univers.Human()
        lb.weight = 0
        lb.grow()
        assert lb.weight == 1, "Grow method doesn't increment weight" 
        result['score'] += 1
    except Exception as e:
        result["errors"].append("4 - %s" % e)

        
def check_exo5(univers, result):
    try:
        lb1 = univers.Human()
        lb2 = univers.Human.give_birth()
        assert type(lb1) == type(lb2), "give_birth doen't do the job." 
        result['score'] += 1
    except Exception as e:
        result["errors"].append("5 - %s" % e)


def check_exo6(univers, result):
    try:
        assert callable(univers.LifeBeing.is_alive), "is_alive n'est pas une methode de class"
        result['score'] += 1
    except Exception as e:
        result["errors"].append("6 - %s" % e)


def check_exo7(univers, result):
    try:

        @univers.my_decorator
        def my_fun():
            pass
        
        assert hasattr(my_fun, 'i_ve_been_decorated'), "docrator did not set attribute."
        result['score'] += 1
    except Exception as e:
        result["errors"].append("7 - %s" % e)


def check_exo8(univers, result):
    try:
        assert isinstance(univers.my_list, list), "my_list n'est pas une list"
        assert isinstance(univers.my_dico, dict), "my_dico n'est pas un dictionnaire"
        result['score'] += 1
    except Exception as e:
        result["errors"].append("8 - %s" % e)

def check_exo9(univers, result):
    try:
        src = inspect.getsource(univers)
        #print(src)
        matches = re.findall(".(if __name__.+)$", src, re.DOTALL)
        assert len(matches) > 0, "No main in the univers module"
        #print(matches[0])
        result["main"] = matches[0]
    except Exception as e:
        result["errors"].append("9 - %s" % e)
    
        

if __name__ == '__main__':

    print("Running test")
    results = {}
    for elem in os.scandir("."):
        if elem.is_dir():
            key = slugify(elem.path)
            results[key] = {
                "score": 0,
                "errors": []
            }
            print("Running test on folder : '%s'" % key)
            spec = importlib.util.spec_from_file_location(
                "%s.univers" % key,
                "%s/univers.py" % elem.path)
            univers = importlib.util.module_from_spec(spec)
            
            try:
                spec.loader.exec_module(univers)
            except Exception as e:
                results[key]["errors"].append(
                    "Cannot import module %s" % e
                )
                continue
                
            check_exo1(univers, results[key])
            check_exo2(univers, results[key])
            check_exo3(univers, results[key])
            check_exo4(univers, results[key])
            check_exo5(univers, results[key])
            check_exo6(univers, results[key])
            check_exo7(univers, results[key])
            check_exo8(univers, results[key])
            check_exo9(univers, results[key])


    for key, value in results.items():
        print("="*80)
        print("%s , score %s" % (key, value["score"]))
        print("Errors %s" % len(value["errors"]))
        for e in value["errors"]:
            print("    %s" % e)

        print("Main:")
        print(value.get('main', 'no main...'))
        
