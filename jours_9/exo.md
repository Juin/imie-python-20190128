# Evaluation module python IMIE 20190207

## Exercice

 * créer un module nommé univers.py

 * definir une fonction multipliant deux nombres appelé 'double' (à la racine du module
   univers, ne pas utiliser input)

 * definir une classe nommée 'LifeBeing' (dans le module univers)
   heritant de la classe ABC du package abc (dans la librairie standard python) 

 * ajouter la variable d'instance 'weight' à cette classe

 * implementer une methode nommée 'grow' incrémentant de '1' la variable d'instance 'weight' 

 * implementer une methode de classe nommée 'give_birth' retournant une nouvelle
   instance de la classe appelante

 * implementer une methode "ABSTRAITE" de classe nommée 'is_alive' retournant le "booléen vrai"

 * créer une classe 'Human' héritant de 'LifeBeing' (cf Q2)
   et implémentant le methode de class is_alive

 * à la racine du module univers, créer un décorateur de fonction nomme 'my_decorator',
   ce décorateur devra ajouter un attribut nommé 'i_ve_been_decorated' à la fonction décorée. 

 * à la racine du module univers, créer une liste vide nommée 'my_list' et un dictionnaire
   vide nommé 'my_dico'

 * enfin, à l'aide de argparse, ajouter un 'main' à votre module universe et faire en
   sorte que l'option '-w ARG' créée une instance de Human et valorise la variable
   d'instance 'weight' de cet Human puis fasse un print de cet Humain

 * Envoyer le module sur benoit@aeon-creation.com