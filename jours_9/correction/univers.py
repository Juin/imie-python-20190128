import abc
import argparse


def double(arg1, arg2):
    return arg1 * arg2


class LifeBeing(abc.ABC):

    def __init__(self):
        self.weight = 0

    def grow(self):
        self.weight += 1

    @classmethod
    @abc.abstractmethod
    def is_alive(cls):
        return True
        
    @classmethod
    def give_birth(cls):
        return cls()


class Human(LifeBeing):
    @classmethod
    def is_alive(cls):
        return True

    
def my_decorator(fun):
    fun.i_ve_been_decorated = "1"
    return fun


my_list = []
my_dico = {}


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Yet another naval battle game')

    parser.add_argument('-w', type=float,
                        dest='weight',
                        help="set weight")

    args = parser.parse_args()

    if args.weight:
        h = Human()
        h.weight = args.weight
        print(h)
