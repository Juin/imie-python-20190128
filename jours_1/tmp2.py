from pprint import pprint 

# Documentation de ma fonction
# arg1: premier argument
# arg2: deuxieme argument
def mon_nom_de_fonction(arg1, arg2, mon_arg=None):
    print("Je suis dans mon_nom_de_fonction")
    print("arg1: %s, arg2: %s" % (arg1, arg2))
    print("arg2: %s" % mon_arg)
    return "done"


def ma_fun(*args, **kwargs):
    global ma_var
    ma_var = 3
    print(ma_var)
    
if __name__ == "__main__":
    help(mon_nom_de_fonction)
