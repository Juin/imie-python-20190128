def do_something_with_vehicul(vehic):
    if vehic is Vehicule:
        vehic.immatriculation += " HH"
    
    return vehic


class Vehicule:
    """ Ceci est la class Vehicule
    """
    
    def __init__(self, immat=None, cylindree="PLOP"):
        """ Constructeur
        """
        self.immatriculation = immat
        self.cylindree = cylindree
        
    def __str__(self):
        return "immatriculation: %s, cylindré: %s" % (self.immatriculation, self.cylindree)

    def __add__(self, other_instance):
        """ How to add 2 vehicules is defined here
        """
        new_immat = "%s / %s" % (self.immatriculation, other_instance.immatriculation)
        return Vehicule(new_immat)

    def klaxon(self):
        print("BEEP!", self)

    @classmethod
    def generate(cls, immat):
        print(cls)
        return Vehicule(immat)


class FlyMixin:

    def __init__(self):
        print("init FlyMixin")
    
    def fly(self):
        print("FlyMixin no you cant")
        
        
class Voiture(Vehicule, FlyMixin):
    
    def __init__(self, nb_roue=4, **kwargs):
        self.nombre_de_roues = nb_roue
        super().__init__(**kwargs)
        
    def show_nbr_roue(self):
        print(self.nombre_de_roues)


if __name__ == "__main__":

    v1 = Voiture(immat="A", cylindree=2000, nb_roue=5)
    print(v1)
    v1.show_nbr_roue()
    v1.fly()

    condition = True
    
    toto = "plop" if condition else "nplop"

    if condition:
        toto = "plop"
    else:
        toto = "nplop"
