from tkinter import Tk, Label, PanedWindow, Widget, Toplevel,\
    Frame, LabelFrame, Button, Canvas, PhotoImage


class MaFrame(Tk):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.batman = PhotoImage(file="batman.png").subsample(4)
        self.geometry("500x500")

    def click_me_collback(self):
        modal = Tk()
        Button(modal, text="B1").pack()
        Button(modal, text="B1").pack()
        Button(modal, text="B1").pack()
        Button(modal, text="B1").pack()
        return 
        
    def set_layout(self):

        top = Label(
            self,
            text="THIS IS ON TOP",
            bg='blue',
            image=self.batman,
            width=10
        )
        top.pack(side='top', fill='x')        

        bottom = Label(self, text="THIS IS ON BOTTOM", bg='blue')
        bottom.pack(side='bottom', fill='x')
        Button(bottom, text="Click me", command=self.click_me_collback).pack()
        
        Label(self, text="THIS IS LEFT SIDED", bg='green').pack(side='left', fill='y')
        Label(self, text="THIS IS RIGHT SIDED", bg='green').pack(side='right', fill='y')

        container = LabelFrame(self, bg='cyan', text='Grille:', borderwidth=1)
        container.pack(side='left', expand=True)
        container.grid_columnconfigure(1, weight=1)

        for i in range(4):
            for j in range(4):
                Label(container,
                      text="%s|%s" % (i, j)
                ).grid(row=i, column=j)

        self.pack_slaves()
        
if __name__ == "__main__":

    mf = MaFrame()
    mf.set_layout()
    mf.mainloop()
