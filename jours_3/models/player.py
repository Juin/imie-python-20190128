import random
from enum import Enum

from models.grid import Grid
from models.ship import Ship


class Orientation(Enum):
    VERTICAL = 0
    HORIZONTAL = 1

class Player:

    def __init__(self, name, config_file_path=None):
        self.name = name
        self.grid = Grid(config_file_path=config_file_path)
        self.shots_grid = Grid(config_file_path=config_file_path)
        self.ships = self.init_ships()

    def __str__(self):
        return self.name
        
    def set_random_ship_position(self, size, orientation=None):
        if orientation == None:
            ori = random.choice([Orientation.HORIZONTAL, Orientation.VERTICAL])
        else:
            ori = orientation

        start = tuple(random.choice(list(self.grid.cells.keys())))
        cells = []
        for i in range(size):

            if ori == Orientation.HORIZONTAL:
                # -- try front then back
                cell = (start[0]-1, start[1])
                if self.grid.cell_is_ok_for_ship(self, cell):
                    cells.append(cell)
                    start = cell
                else:
                    cell = (start[0]+len(cells), start[1])
                    if self.grid.cell_is_ok_for_ship(self, cell):
                        cells.append(cell)
                    else:
                        # -- we're stuck retry from start
                        cells.clear()
                        break            
            else:
                # -- try front then back
                cell = (start[0], start[1]-1)
                if self.grid.cell_is_ok_for_ship(self, cell):
                    cells.append(cell)
                    start = cell
                else:
                    cell = (start[0], start[1]+len(cells))
                    if self.grid.cell_is_ok_for_ship(self, cell):
                        cells.append(cell)
                    else:
                        # -- we're stuck retry from start
                        cells.clear()
                        break

        if len(cells) != size:
            cells = self.set_random_ship_position(size)
        return cells
        
            
    def init_ships(self):
        ships = []
        self.grid.put_ships(ships)

        for name, size in [('pa', 5), ('cr', 4), ('ct', 3),
                           ('sm', 3), ('to', 2)]:
            cells = self.set_random_ship_position(size)
            ships.append(Ship(name, cells))
            
        return ships

    def shoot_to_ennemi(self, coords, is_ploof):
        self.shots_grid.add_shoot(coords, is_ploof)

    def shoot_from_ennemi(self, coords):
        ship = self.grid.get_ship(coords)
        if ship is not None:
            self.grid.cells[coords] = "-"
        return ship is None 
