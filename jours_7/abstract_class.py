from abc import ABC, abstractmethod


class C(ABC):
    @abstractmethod
    def my_abstract_method(self, *args):
        pass
        
    @classmethod
    @abstractmethod
    def my_abstract_classmethod(cls, *args):
        pass
    
    @staticmethod
    @abstractmethod
    def my_abstract_staticmethod(*args):
        pass
    
    @property
    @abstractmethod
    def my_abstract_property(self):
        pass
    
    
class A(C):
    def my_abstract_method(self, *args):
        pass
    

class B(C):
    def my_abstract_method(self, *args):
        pass
        
    @classmethod
    def my_abstract_classmethod(cls, *args):
        pass
    
    @staticmethod
    def my_abstract_staticmethod(*args):
        pass
    
    @property
    def my_abstract_property(self):
        pass
    

if __name__ == "__main__":
    a = A()
    print(a)

    b = B()
    print(b)
