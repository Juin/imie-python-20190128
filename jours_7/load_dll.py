import ctypes

# Load DLL into memory.

test_dll = ctypes.WinDLL("test.dll")

return_2_prototype = ctypes.WINFUNCTYPE(
    ctypes.c_int
)

return_2_function_in_dll = return_2_prototype(("return_2", test_dll))
#arg = ctypes.c_int(1)

result = return_2_function_in_dll()
print(result)
