import socket
import signal
import time
from threading import Thread


class ClientThread(Thread):

    def __init__(self,
                 connection=None,
                 address=None,
                 server=None):

        self.should_run = True
        self.connection = connection
        self.address = address
        self.server = server
        self.buffer = None
        super().__init__()

    def run(self):
        print("Client thread started")
        while self.should_run:

            try:
                data = self.connection.recv(1024)
                if self.buffer is None:
                    self.buffer = data
                else:
                    self.buffer += data
                #print(self.buffer, data[-1], str(data[-1]))
                if len(data) == 0:
                    self.should_run = False
                elif data[-1] == 10:
                    self.server.send_all(self.buffer)
                    self.buffer = None
            except ConnectionAbortedError as e:
                print("Connection has been closed while recv call")
            except ConnectionResetError as e:
                print("Connection has been remotly closed while recv call")
                break
            
        self.server.disconnect(self)
        self.connection.close()
            
    def send(self, data):
        self.connection.send(data)
        
    def stop_thread(self):
        self.should_run = False
        self.connection.close()

        
class Server():

    def __init__(self, host=None, port=None):
        self.host = host
        self.port = port
        self.should_stop = False
        self.clients = []
        self.socket = None

    def disconnect(self, cli):
        print("Client disconnected")
        self.clients.remove(cli)
        
    def shutdown(self, *args):
        print("shutdown", args)
        for cli in self.clients:
            cli.stop_thread()
        self.should_stop = True
        self.socket.close()
        
    def send_all(self, msg):
        print("send_all", msg)
        for cli in self.clients:
            cli.send(msg)

    def wait_for_clients(self):
        while not self.should_stop:
            try:
                conn, addr = self.socket.accept()
                print("connection accepted")
                cli = ClientThread(connection=conn,
                                   address=addr,
                                   server=self)
                cli.start()
                self.clients.append(cli)
            except OSError as e:
                print("Accept exception catched on closed socket")
            
    def launch(self):    
        with socket.socket(socket.AF_INET,
                           socket.SOCK_STREAM) as s:
            self.socket = s
            s.bind((self.host, self.port))
            s.listen()

            self.wait_for_clients_thread = Thread(
                target = self.wait_for_clients,
                args = ())

            self.wait_for_clients_thread.start()
            
            while True:
                try:
                    time.sleep(1)
                    print('\rWaitting for clients, actual number of clients: %s' % \
                          len(self.clients),end="")
                except KeyboardInterrupt: 
                    print('Shutting down')
                    self.shutdown()
                    break

class Client():

    def __init__(self, host=None, port=None):
        self.host = host
        self.port = port
        self.should_stop = False
        self.socket = None

    def kill(self):
        self.should_stop = True
        s.close()
        
    def print_msg_recv(self, data):
        while not self.should_stop:
            try:
                data = self.socket.recv(1024)
                print(data)
            except ConnectionAbortedError as e:
                print("Connection has been closed while recv call")
            
    def launch(self):
        with socket.socket(socket.AF_INET,
                           socket.SOCK_STREAM) as s:
            self.socket = s
            s.connect((self.host, self.port))

            thread = Thread(
                target = self.print_msg_recv,
                args = (self,))
            #thread.start()
            
            while not self.should_stop:
                try:
                    msg = input("msg ?")
                    msg += "\n"
                    s.send(msg.encode('utf-8'))
                except KeyboardInterrupt: 
                    print("client shutdown")
                    self.should_stop = True
                    self.socket.close()
                except ConnectionResetError as e:
                    print("Connection cosed, server may have been killed")
                    
