using namespace std;
#include <iostream>
#include "stdafx.h"

BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved )
{
   return TRUE;
}

extern "C" __declspec(dllexport) int return_2()
{
    return 2;
}

void main()
{
  cout << "Hello, world, from Visual C++!" <<  return_2() << endl;
}
