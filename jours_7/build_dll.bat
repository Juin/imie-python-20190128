REM set PATH=%PATH%;
REM set PATH=%PATH%;"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\amd64"

REM BUILD EXE:
REM cl /I "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\include"^
REM    /I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.10240.0\ucrt"^
REM    /EHsc test.cpp^
REM    /link /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\lib\amd64"^
REM          /LIBPATH:"C:\Program Files (x86)\Windows Kits\8.0\Lib\win8\um\x64"^
REM          /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.10240.0\ucrt\x64"

REM BUILD DLL:
cl /I "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\atlmfc\include"^
   /I "C:\Program Files (x86)\Windows Kits\8.1\Include\shared"^
   /I "C:\Program Files (x86)\Windows Kits\8.1\Include\um"^
   /I "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\atlmfc\src\mfc"^
   /I "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\include"^
   /I "C:\Program Files (x86)\Windows Kits\10\Include\10.0.10240.0\ucrt"^
   /LD test.cpp ^
   /link /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\lib\amd64"^
         /LIBPATH:"C:\Program Files (x86)\Windows Kits\8.0\Lib\win8\um\x64"^
	 /LIBPATH:"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.10240.0\ucrt\x64"^
	 /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\atlmfc\lib\amd64" 

