https://gitlab.com/Juin/imie-python-20190128

# Cours Python IMIE 2019-01-28

## Lien de cours

* https://python.developpez.com/tutoriels/apprendre-programmation-python/les-bases/?page=ordinateur-et-programmation
* http://book.pythontips.com/en/latest/decorators.html

## Jours 1 - généralitées sur python

* Variable
* Portée
* Fonctions
* Classes
* Listes
* Dictionnaires

## Jours 2 - TP: bataille navale

Mise en pratique des notions:

* Classes
* Lambda
* map
* dico
* liste
* input
* for
* fonctions
* UML - (Use case, diagramme de classes, diagramme de séquence)

## Jours 3 - Connecteur BDD, suite TP: bataille navale

* argparse
     Utiliser argparse dans votre main.py, pour avoir l'aide.
     python main.py -h
         "Afficher l'aide"
     python maine.py -c chemin/du/fichier/config.json
         Lance le programme et charge le fichier passé en parametre

* sqlalchemy
   * Ajouter une option (avec argparse) pour afficher les High-Score
   * Les high-score seront stockés dans une table sqlite.
   * Il faudra simplement lire la table avec sqlAlchemy, puis afficher les
     lignes de cette table
   * Puis afficher les high_score de la base en utilisant l'ORM de sqlalchemy

* socket
   * Commencer la couche reseau pour le jeu
   * Doc socket: https://docs.python.org/3/library/socket.html#example
   * https://docs.python.org/3/howto/sockets.html#socket-howto
   * Ajouter une option mode_server
   * Ajouter une option mode_client
   * Définir le protocol

## Jours 4 - Python & WEB: Flask, Bottle, Django

* Flask
   * Afficher la table des scores (TP bataille navale)
     dans une page web avec Flask et Sqlalchemy
* Django
   * Faire un gestionnaire de commande.
      * Un model Customer
      * Un model Article
      * Un model Commande
      * Afficher une commande
      * Utiliser DjangoAdmin pour saisir les données.

## Jours 5 - Suite TP: Bataille Navaele,
   * Utilisation GIT
   * Implementation complete de la boucle de jeux
   * Implementation de la couche reseau
   * Implementation de la couche graphique


## Jours 6 - TP: Super Hero Rampage
   * Notions
	- Threading - Socket
   	- Exception
   	- Meta Classes & type
   * TP: Jeu de super heros
        - class Hero
           - hp (health points)
	   - ap (action points)
	   - position
	   - orientation 
           - name
 	   - actions possibles: walk, fly, punch, fire
        - class Map
           - permettre l'affichage de la carte avec print()
        - class Engine
        - fichier de config
           - format: ini, json, xml au choix
	   - cout/action
	   - taille map
	   - nb joueurs
	   - nb action au départ
	   - % batiment/case_map   

## Jours 7 - Suite TP: Hero Rampage
    - Notions:
        - multiprocessing
	- shutils
	- socket/thread suite: gerer les erreurs (déconnexion, ctrl+c)
	- module 'abc', classes et methodes abstraite (interface C#?)
	- chargement de dll dans python et appel de fonction de la dll
	- partial (package: functools)

## Jours 8 - Suite TP: Hero rampage
    - Notions:
    - Evaluation:
        - A: Acquis avec très grande maitrise-expert (18-20/20)
	- B: Acquis avec grande maitrise (16-18/20)
	- C: Acquis avec bonne maitrise (14-16/20)
	- D: Acquis avec de solides bases (12-14/20)
	- E: Acquis avec les bases (10-12/20)
	- Fx: Non acquis mais a fourni beaucoup d'efforts (7-10/20)
	- F: Non acquis et n'a fourni aucun effort (- de 10/20 - attitude négative)

## Jours * - PyGtk, PyQT

## Jours * - NumPy

## Jours * - Plotly

## Jours * - Graphen

## Jours * - PyGame

## Jours * - asyncio
