class Meta(type):
    def __new__(cls, name, mother_classes, dict_attributes):
        x = super().__new__(cls, name, mother_classes, dict_attributes)
        x.a = "A"
        return x


class ClassFoo(metaclass=Meta):
    pass


class Mother():
    def common_methode(self):
        print("common_methode")

        
class Son(Mother):
    pass
    

if __name__ == "__main__":
    cf = ClassFoo()
    print(cf)
    print(type(cf))
    print(cf.a)
