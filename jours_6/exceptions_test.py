class CustomError(Exception):
    pass

if __name__ == "__main__":


    try:
        #oo = {}
        #oo["cle"]["sous_cle"] = "val"
        #a.y = "A"
        raise CustomError("AA")
    except KeyError as e:
        print("Acces a une clé non existante?")
    except NameError as e:
        print("Il ya surement qqch de non defini ici!")
        raise Exception("Erreur 8976")
    except CustomError as e:
        print("Erreur custom, ...")
    except Exception as e:
        print(type(e))
        print("L'exception est : '%s'" % e)
    finally:
        print("Executé toujours")
