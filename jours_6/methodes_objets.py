

class MaClass():
    def __init__(self):
        self.var_instance = "A"

    def __getattr__(self, attr_name):
        print(attr_name)
        return "__getattr__: Valeur auto de %s" % attr_name

    def __setattr__(self, attr_name, val_attr):
        
        print("__setattr__: Acces a l'attribut: %s , valeur: %s" % \
              (attr_name, val_attr))

    
if __name__ == "__main__":
    instance = MaClass()
    print(instance.var_instance)
    print(instance.attribut_non_defini_dans_la_classe)

    instance.var_instance = "B"
    print("instance.var_instance = %s" % instance.var_instance)
    instance.autre_attribut_non_defini = "B"
    print("instance.autre_attribut_non_defini = %s" % \
          instance.autre_attribut_non_defini)
