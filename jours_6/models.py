import socket
import signal
from threading import Thread


class ClientThread(Thread):

    def __init__(self,
                 connection=None,
                 address=None,
                 server=None):

        self.should_run = True
        self.connection = connection
        self.address = address
        self.server = server
        self.buffer = None
        super().__init__()

    def run(self):
        while self.should_run:
            
            data = self.connection.recv(1024)
            if self.buffer is None:
                self.buffer = data
            else:
                self.buffer += data
            #print(self.buffer, data[-1], str(data[-1]))
            if data[-1] == 10:
                self.server.send_all(self.buffer)
                self.buffer = None
            
    def send(self, data):
        self.connection.send(data)
            

class Server():

    def __init__(self, host=None, port=None):
        self.host = host
        self.port = port
        self.should_stop = False
        self.clients = []
        self.socket = None

    def kill(self, *args):
        print("kill", args)
        self.socket.close()
        
    def send_all(self, msg):
        print("send_all", msg)
        for cli in self.clients:
            cli.send(msg)
        
    def launch(self):    
        with socket.socket(socket.AF_INET,
                           socket.SOCK_STREAM) as s:
            self.socket = s
            s.bind((self.host, self.port))
            s.listen()
            while not self.should_stop:
                conn, addr = s.accept()    
                cli = ClientThread(connection=conn,
                                   address=addr,
                                   server=self)
                cli.start()
                self.clients.append(cli)
                

class Client():

    def __init__(self, host=None, port=None):
        self.host = host
        self.port = port
        self.should_stop = False
        self.socket = None

    def kill(self):
        self.should_stop = True
        
    def print_msg_recv(self, data):
        while not self.should_:
            data = self.socket.recv(1024)
            print(data)
        
    def launch(self):
        with socket.socket(socket.AF_INET,
                           socket.SOCK_STREAM) as s:
            self.socket = s
            s.connect((self.host, self.port))

            thread = Thread(
                target = self.print_msg_recv,
                args = (self,))
            thread.start()
            
            while not self.should_stop:
                msg = input("msg ?")
                msg += "\n"
                s.send(msg.encode('utf-8'))
                
        
