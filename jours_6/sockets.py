import signal
import argparse
import sys

from models import Client, Server


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Global Chat Cast')
    parser.add_argument('-s', dest='server',
                        action='store_true',
                        help="Launch as server")    
    parser.add_argument('-c', dest='client',
                        action='store_true',
                        help="Launch as client")    
    parser.add_argument('-H', dest='host',
                        help="Launch as client",
                        default="127.0.0.1")    
    parser.add_argument('-p', dest='port',
                        help="Launch as client",
                        default=5050,
                        type=int)    

    args = parser.parse_args()

    print(args)
    
    if args.server:
        s = Server(host=args.host, port=args.port)
        
        s.launch()
        
        
        
    elif args.client:
        c = Client(host=args.host, port=args.port)
        signal.signal(signal.SIGINT, c.kill)
        
        c.launch()
