import threading
from time import sleep


class MonThread(threading.Thread):

    def __init__(self, name, speed):
        self.counter = 0
        self.speed = speed
        self.my_name = name
        super().__init__()

    def run(self):
        while self.counter < 10:
            print("%s: counter = %s" % (self.my_name, self.counter))
            sleep(1/self.speed)
            self.counter += 1

            
if __name__ == "__main__":
    t1 = MonThread("AA", 1)
    t2 = MonThread("B", 3)

    t1.start()
    t2.start()

    t1.join()
    t2.join()
