import random
import re

from models.player import Player


class Engine:

    def __init__(self):
        self.players = self.initialize()
        self.current_player = None
        self.opponent = None
        self.winner = None
        
    def initialize(self):
        players = []
        player_name = input("Player 1 name?\n")
        players.append(Player(player_name))
        player_name = input("Player 1 name?\n")
        players.append(Player(player_name))
        return players 

    
    def start(self):
        while self.winner is None:
            
            if self.current_player is None:
                self.current_player = random.choice(self.players)
            else:
                self.current_player = self.opponent
            self.opponent = self.players[(self.players.index(self.current_player)+1) % 2]
            
            print(self.current_player)
            print(self.current_player.grid)
            
            print(self.opponent)
            print(self.opponent.grid)
            
            
            
            position = None
            while position is None:
                position = input("Player %s choose a target\n" % self.current_player)
                matches = re.match('([A-Z])([0-9])', position)
                if matches is not None:
                    line, column = matches.groups()
                    column = int(column)
                    line = ord(line) - 65
                    print(line)
                    position = (line, column)

            is_ploof = self.opponent.shoot_from_ennemi(position)
            self.current_player.shoot_to_ennemi(position, is_ploof)
   
